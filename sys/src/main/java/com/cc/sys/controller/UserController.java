package com.cc.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cc.common.R;
import com.cc.common.utils.ServletUtils;
import com.cc.common.utils.UserInfoContext;
import com.cc.sys.api.domain.SysUser;
import com.cc.sys.service.IUserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author whf
 * @since 2024/2/29 18:33
 **/
@RestController
@RequestMapping("user")
@Tag(name = "用户控制器")
public class UserController {

    @Autowired
    IUserService userService;

    /**
     * 通过用户名查询用户信息
     *
     * @param username 用户名
     * @param source   请求来源
     * @return 结果
     */
    @GetMapping("info/{username}")
    public R<?> getUserInfo(@PathVariable("username") String username) {
        return R.ok(userService.getOne(new QueryWrapper<SysUser>().eq("user_name", username)));
    }

    @GetMapping("getUser")
    public R<SysUser> getUser(@RequestParam Long id) {
        return R.ok(userService.getById(id));
    }


    /**
     * 注册用户信息
     *
     * @param sysUser 用户信息
     * @param source  请求来源
     * @return 结果
     */
    @PostMapping("/register")
    public R<Boolean> registerUserInfo(@RequestBody SysUser sysUser) {

        return R.ok(userService.save(sysUser));
    }


    @GetMapping("info")
    public R<?> getUserInfo(HttpServletRequest request) {
        System.out.println("userService.getById(ServletUtils.getUserId(request)) = " + userService.getById(ServletUtils.getUserId(request)));
        return R.ok(userService.getById(ServletUtils.getUserId(request)));
    }
}
