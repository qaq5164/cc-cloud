package com.cc.sys.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户和角色关联表 前端控制器
 *
 * @author whf
 * @since 2024-03-05 09:46
 */
@RestController
@RequestMapping("/sys-user-role")
public class SysUserRoleController {

}
