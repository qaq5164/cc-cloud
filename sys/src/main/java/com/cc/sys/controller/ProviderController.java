package com.cc.sys.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author whf
 * @since 2023/12/9 11:09
 **/

@RestController
@Tag(name = "asd")
public class ProviderController {


    @GetMapping(value = "/hi")
    public String sayHi() {
        return "Hello ";
    }

    @GetMapping(value = "/test/{message}")
    public String test(@PathVariable("message") String message){
        return message;
    }

}