package com.cc.sys.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 菜单权限表 前端控制器
 *
 * @author whf
 * @since 2024-03-05 09:51
 */
@RestController
@RequestMapping("/sys-menu")
public class SysMenuController {

}
