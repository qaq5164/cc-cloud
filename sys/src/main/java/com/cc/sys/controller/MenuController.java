package com.cc.sys.controller;

import com.cc.common.R;
import com.cc.sys.api.domain.SysMenu;
import com.cc.sys.service.IRoleMenuService;
import com.cc.sys.service.IUserRoleService;
import com.cc.sys.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 角色和菜单关联表 前端控制器
 *
 * @author whf
 * @since 2024-03-05 09:37
 */
@RestController
@RequestMapping("menu")
public class MenuController {

    @Autowired
    IRoleMenuService roleMenuService;
    @Autowired
    IUserRoleService userRoleService;

    @GetMapping("list")
    public R<?> list() {
        Long roleId = userRoleService.getRole();
        List<SysMenu> menuList = roleMenuService.getMenuByRoleId(roleId);
        return R.ok(menuList);
    }


}
