package com.cc.sys.mapper;

import org.apache.ibatis.annotations.Mapper;
import com.cc.sys.api.domain.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* 菜单权限表 Mapper
*
* @author whf
* @since 2024-03-05 09:51
*/
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {
}
