package com.cc.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cc.sys.api.domain.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author whf
 * @since 2024/2/29 18:35
 **/
//@Mapper
public interface UserMapper extends BaseMapper<SysUser> {
}
