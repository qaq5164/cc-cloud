package com.cc.sys.mapper;

import com.cc.sys.api.domain.SysMenu;
import com.cc.sys.api.domain.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* 角色和菜单关联表 Mapper
*
* @author whf
* @since 2024-03-05 09:37
*/

public interface RoleMenuMapper extends BaseMapper<SysRoleMenu> {
    List<SysMenu> getMenuByRoleId(Long roleId);
}
