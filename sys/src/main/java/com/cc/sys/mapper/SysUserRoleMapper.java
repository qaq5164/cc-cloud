package com.cc.sys.mapper;

import org.apache.ibatis.annotations.Mapper;
import com.cc.sys.api.domain.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* 用户和角色关联表 Mapper
*
* @author whf
* @since 2024-03-05 09:46
*/
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
}
