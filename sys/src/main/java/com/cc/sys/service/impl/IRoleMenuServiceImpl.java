package com.cc.sys.service.impl;

import com.cc.sys.api.domain.SysMenu;
import com.cc.sys.service.IRoleMenuService;
import com.cc.sys.api.domain.SysRoleMenu;
import com.cc.sys.mapper.RoleMenuMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 角色和菜单关联表 服务实现类
 *
 * @author whf
 * @since 2024-03-05 09:37
 */
@Service
public class IRoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, SysRoleMenu> implements IRoleMenuService {

    @Override
    public List<SysMenu> getMenuByRoleId(Long roleId) {
        return baseMapper.getMenuByRoleId(roleId);
    }
}
