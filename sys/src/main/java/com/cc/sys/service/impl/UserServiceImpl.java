package com.cc.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cc.sys.api.domain.SysUser;
import com.cc.sys.mapper.UserMapper;
import com.cc.sys.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * @author whf
 * @since 2024/2/29 18:43
 **/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, SysUser> implements IUserService {

}
