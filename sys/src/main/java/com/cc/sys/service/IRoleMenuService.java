package com.cc.sys.service;

import com.cc.sys.api.domain.SysMenu;
import com.cc.sys.api.domain.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 角色和菜单关联表 服务类接口
 *
 * @author whf
 * @since 2024-03-05 09:37
 */
public interface IRoleMenuService extends IService<SysRoleMenu> {

    List<SysMenu> getMenuByRoleId(Long roleId);
}
