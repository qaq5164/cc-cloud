package com.cc.sys.service;

import com.cc.sys.api.domain.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户和角色关联表 服务类接口
 *
 * @author whf
 * @since 2024-03-05 09:46
 */
public interface IUserRoleService extends IService<SysUserRole> {

    Long getRole();
}
