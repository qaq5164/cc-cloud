package com.cc.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cc.sys.api.domain.SysRole;
import com.cc.sys.api.domain.SysUser;

/**
 * @author whf
 * @since 2024/2/29 18:41
 **/
public interface IUserService extends IService<SysUser> {

}
