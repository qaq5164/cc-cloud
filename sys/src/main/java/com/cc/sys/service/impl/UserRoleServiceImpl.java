package com.cc.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cc.common.utils.UserInfoContext;
import com.cc.sys.service.IUserRoleService;
import com.cc.sys.api.domain.SysUserRole;
import com.cc.sys.mapper.SysUserRoleMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 用户和角色关联表 服务实现类
 *
 * @author whf
 * @since 2024-03-05 09:46
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements IUserRoleService {

    @Override
    public Long getRole() {
        return baseMapper.selectOne(new QueryWrapper<SysUserRole>().eq("", UserInfoContext.getUserId())).getRoleId();
    }
}
