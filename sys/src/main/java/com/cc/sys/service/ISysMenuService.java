package com.cc.sys.service;

import com.cc.sys.api.domain.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 菜单权限表 服务类接口
 *
 * @author whf
 * @since 2024-03-05 09:51
 */
public interface ISysMenuService extends IService<SysMenu> {

}
