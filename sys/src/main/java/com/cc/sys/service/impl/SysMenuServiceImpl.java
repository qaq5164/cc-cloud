package com.cc.sys.service.impl;

import com.cc.sys.service.ISysMenuService;
import com.cc.sys.api.domain.SysMenu;
import com.cc.sys.mapper.SysMenuMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 菜单权限表 服务实现类
 *
 * @author whf
 * @since 2024-03-05 09:51
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

}
