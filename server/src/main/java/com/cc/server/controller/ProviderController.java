package com.cc.server.controller;

import com.cc.server.service.FeignService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author whf
 * @since 2023/12/9 11:09
 **/

@RestController
@Tag(name = "sayHello")
public class ProviderController {


    @Autowired
    private FeignService feignService;
    @GetMapping(value = "/hi")
    public String sayHi() {
        return "Hello ";
    }

    @GetMapping(value = "/f")
    public String feignTest() {
        return feignService.test("asd");
    }

}