package com.cc.common.configure;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.cc.common.utils.JwtUtils;
import com.cc.common.utils.UserInfoContext;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * mybatisplus字段自动填充配置
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {


    /**
     * 插入填充指定的MetaObject对象
     *
     * @param metaObject 要插入填充的MetaObject对象
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        // 使用严格模式插入填充指定的MetaObject对象的属性值

        this.strictInsertFill(metaObject, "createTime", LocalDateTime.class, LocalDateTime.now());
        this.strictInsertFill(metaObject, "createBy", String.class, UserInfoContext.getUserName());
        this.strictInsertFill(metaObject, "optBy", String.class,  UserInfoContext.getUserName());
        this.strictInsertFill(metaObject, "createDate",LocalDateTime.class, LocalDateTime.now());

    }

    @Override
    public void updateFill(MetaObject metaObject) {

        this.strictUpdateFill(metaObject, "loginTime", LocalDateTime::now, LocalDateTime.class); // 起始版本 3.3.3(推荐)
        this.strictUpdateFill(metaObject, "update_time", LocalDateTime::now, LocalDateTime.class);
        this.strictInsertFill(metaObject, "updateBy", String.class, UserInfoContext.getUserName());
    }
}